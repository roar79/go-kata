package main

import (
	"testing"
)

func TestAverage(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name         string
		args         args
		wantAverage  float64
		wantSum      float64
		wantDivide   float64
		wantMultiply float64
	}{
		{
			name:         "Test  #1",
			args:         args{a: 0, b: 1},
			wantAverage:  0.5,
			wantSum:      1,
			wantDivide:   0,
			wantMultiply: 0,
		},
		{
			name:         "Test  #2",
			args:         args{a: 1, b: 2},
			wantAverage:  1.5,
			wantSum:      3,
			wantDivide:   0.5,
			wantMultiply: 2,
		},
		{
			name:         "Test  #3",
			args:         args{a: -2.5, b: 2.5},
			wantAverage:  0,
			wantSum:      0,
			wantDivide:   -1,
			wantMultiply: -6.25,
		},
		{
			name:         "Test  #4",
			args:         args{a: 5.5, b: 9.5},
			wantAverage:  7.5,
			wantSum:      15,
			wantDivide:   0.5789473684210527,
			wantMultiply: 52.25,
		},
		{
			name:         "Test  #5",
			args:         args{a: 6.8, b: 3.2},
			wantAverage:  5,
			wantSum:      10,
			wantDivide:   2.125,
			wantMultiply: 21.76,
		},
		{
			name:         "Test  #6",
			args:         args{a: 258, b: -0.25},
			wantAverage:  128.875,
			wantSum:      257.75,
			wantDivide:   -1032,
			wantMultiply: -64.5,
		},
		{
			name:         "Test  #7",
			args:         args{a: 555, b: 55.5},
			wantAverage:  305.25,
			wantSum:      610.5,
			wantDivide:   10,
			wantMultiply: 30802.5,
		},
		{
			name:         "Test  #8",
			args:         args{a: 0.852, b: 0.888},
			wantAverage:  0.87,
			wantSum:      1.74,
			wantDivide:   0.9594594594594594,
			wantMultiply: 0.756576,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.args.a, tt.args.b); got != tt.wantAverage {
				t.Errorf("average() = %v, want %v", got, tt.wantAverage)
			}
		})
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.args.a, tt.args.b); got != tt.wantMultiply {
				t.Errorf("average() = %v, want %v", got, tt.wantMultiply)
			}
		})
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.wantDivide {
				t.Errorf("average() = %v, want %v", got, tt.wantDivide)
			}
		})
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.args.a, tt.args.b); got != tt.wantSum {
				t.Errorf("average() = %v, want %v", got, tt.wantSum)
			}
		})
	}

}
