package main

import "unicode"

var CirillicLatin = map[rune]string{
	'А': "A",
	'Б': "B",
	'В': "V",
	'Г': "G",
	'Д': "D",
	'Е': "E",
	'Ё': "YO",
	'Ж': "ZH",
	'З': "Z",
	'И': "I",
	'Й': "J",
	'К': "K",
	'Л': "L",
	'М': "M",
	'Н': "N",
	'О': "O",
	'П': "P",
	'Р': "R",
	'С': "S",
	'Т': "T",
	'У': "U",
	'Ф': "F",
	'Х': "H",
	'Ц': "C",
	'Ч': "CH",
	'Ш': "SH",
	'Щ': "SCH",
	'Ъ': "'",
	'Ы': "Y",
	'Ь': "",
	'Э': "E",
	'Ю': "JU",
	'Я': "JA",
	'а': "a",
	'б': "b",
	'в': "v",
	'г': "g",
	'д': "d",
	'е': "e",
	'ё': "yo",
	'ж': "zh",
	'з': "z",
	'и': "i",
	'й': "j",
	'к': "k",
	'л': "l",
	'м': "m",
	'н': "n",
	'о': "o",
	'п': "p",
	'р': "r",
	'с': "s",
	'т': "t",
	'у': "u",
	'ф': "f",
	'х': "h",
	'ц': "c",
	'ч': "ch",
	'ш': "sh",
	'щ': "sch",
	'ъ': "'",
	'ы': "y",
	'ь': "",
	'э': "e",
	'ю': "ju",
	'я': "ja",
}

func RuToTranslit(sliceRune []rune, CirillicLatin map[rune]string) []rune {
	var a string
	for _, r := range sliceRune {
		if unicode.Is(unicode.Cyrillic, r) {
			a += CirillicLatin[r]

		} else {
			a += string(r)
		}
	}
	sliceRune = []rune(a)
	return sliceRune

}
func DeleteEmoji(sliceRune []rune) []rune {
	var TextWithoutEmoji string

	a := '🙂'
	b := "=)"
	for i := range sliceRune {
		if sliceRune[i] == a {
			TextWithoutEmoji += b

		} else {
			TextWithoutEmoji += string(sliceRune[i])
		}

	}
	sliceRune = []rune(TextWithoutEmoji)

	return sliceRune

}
func RuneToByte(sliceRune []rune) string {
	result := string(sliceRune)
	return result

}
