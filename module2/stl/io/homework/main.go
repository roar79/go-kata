package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
)

func chek(err error) {
	if err != nil {
		log.Fatal(err)
	}

}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	// создаем буфер
	buf := bytes.Buffer{}
	// записываем данные в буфер
	for _, d := range data {
		buf.WriteString(d + "\n")
	}

	// создаем файл
	file, err := os.Create("example2.txt")
	chek(err)

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			panic(err)
		}
	}(file)

	// записываем данные в файл
	_, err = io.Copy(file, &buf)
	chek(err)

	// читаем данные из файла в новый буфер
	newBuf := bytes.Buffer{}
	file, err = os.Open("example2.txt")
	chek(err)

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			panic(err)
		}
	}(file)

	_, err = io.Copy(&newBuf, file)
	chek(err)

	// выводим данные из нового буфера
	fmt.Println(newBuf.String())
}
