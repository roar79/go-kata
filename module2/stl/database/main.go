package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func main() {

	database, _ := sql.Open("sqlite3", "./gopher.db")
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people(id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, _ = statement.Exec()

	statement, _ = database.Prepare("INSERT INTO people(firstname, lastname)VALUES (?,?)")
	_, _ = statement.Exec("Polina", "Tihonova")

	rows, _ := database.Query("SELECT id, firstname,lastname FROM people")
	var id int
	var firstname string
	var lastname string
	for rows.Next() {
		_ = rows.Scan(&id, &firstname, &lastname)
		fmt.Printf("%d: %s\n", id, firstname)
	}

}
