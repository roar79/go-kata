package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/essentialkaos/translit"
)

func main() {
	// Создание нового файла
	file, err := os.Create("text.txt")
	if err != nil {
		fmt.Println("Ошибка при создании файла:", err)
		return
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println("Ошибка при закрытии  файла:", err)

		}
	}(file)

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')

	input := fmt.Sprintf("Hello %s\n", name)

	_, err = file.WriteString(input)
	if err != nil {
		fmt.Println("Ошибка при записи в файл:", err)
		return
	}
	fmt.Println("Файл успешно создан и записан.")

	exampleText, err := os.ReadFile("example.txt")
	if err != nil {
		fmt.Println("Ошибка при чтении файла:", err)
		return

	}
	exampleTextTranslit := translit.EncodeToBGN(string(exampleText))

	file, err = os.Create("example.processed.txt")
	if err != nil {
		fmt.Println("Ошибка при создании файла:", err)
		return
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println("Ошибка при закрытии  файла:", err)

		}
	}(file)
	_, err = file.WriteString(exampleTextTranslit)
	if err != nil {
		fmt.Println("Ошибка при записи в файл:", err)
		return
	}
	fmt.Println("Файл успешно создан и записан.")

}
