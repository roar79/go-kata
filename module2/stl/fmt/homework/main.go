package main

import f "fmt"

type Person struct {
	Name  string
	Age   int
	Money float64
}

func (p Person) generateSelfStory() {
	f.Printf("Hello! My name is %s. I'm %d y.o. And I also have $%.2f in may wallet right now.\n", p.Name, p.Age, p.Money)

}

func main() {
	a := Person{Name: "Andy", Age: 18, Money: 199.22}

	a.generateSelfStory()

	// вывод значений структуры
	f.Println("simple struct:", a)

	// вывод названий полей и их значений
	f.Printf("detailed struct: %+v\n", a)

	// вывод названий полей и их значений в виде инициализации
	f.Printf("Golang struct: %#v\n", a)
}

//Задание
//
//Реализуй функцию generateSelfStory(name string, age int, money float64) string, которая генерирует предложение, подставляя переданные данные в возвращаемую строку. Например:
//
//
//
//Пример решения ты можешь посмотреть по ссылке.
