package main

//Задание

//Создай консольную утилиту, принимающую путь конфигурационного файла в качестве флага, формат файла json, выводящая настройки конфигурационного файла:
//./configapp -conf ./config.json
//Создай валидный config.json согласно структуре в коде.
//Пропиши тэги json для структуры.
//Произведи декодирование в структуру.
//Выведи текст в консоль согласно примеру, приведенному по ссылке.

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"os"

	"github.com/brianvoe/gofakeit/v6"
)

var fileName = "config.json"

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func creatJson() {
	_, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Ошибка при создании файла:", err)
		return
	}

}
func writeJson() {
	a, err := os.Open(fileName)
	if err != nil {
		fmt.Println("Ошибка открытия файла")
		return
	}
	buf := new(bytes.Buffer)
	bufEncoder := json.NewEncoder(buf)
	_ = bufEncoder.Encode(Config{AppName: gofakeit.AppName(), Production: gofakeit.Bool()})
	err = os.WriteFile(fileName, buf.Bytes(), 0644)
	if err != nil {
		fmt.Println("Ошибка записи в файл", err)
		return
	}
	_ = a.Close()

	fmt.Println("Файл успешно записан")
}

func main() {

	creatJson()
	writeJson()

	configFileFlag := flag.String("conf", "", "путь до файла")
	flag.Parse()

	if *configFileFlag == "" {
		fmt.Println("Необходимо указать путь к файлу")
		os.Exit(1)
	}

	configFile, err := os.Open(*configFileFlag)
	if err != nil {
		fmt.Println("Ошибка открытия файла:", err)
		return
	}
	defer func(configFile *os.File) {
		err := configFile.Close()
		if err != nil {
			panic(err)

		}
	}(configFile)

	configScanner := bufio.NewScanner(configFile)
	var configBytes []byte
	for configScanner.Scan() {
		configBytes = append(configBytes, configScanner.Bytes()...)
	}
	if err := configScanner.Err(); err != nil {
		panic(err)
	}
	//fmt.Println(configBytes)
	if !json.Valid(configBytes) {
		panic("Не валидный JSON")
	}
	var conf Config
	_ = json.Unmarshal(configBytes, &conf)
	if err != nil {
		panic(err)

	}
	fmt.Println("App name:", conf.AppName)
	fmt.Println("Production:", conf.Production)
	fmt.Println(conf)
}
