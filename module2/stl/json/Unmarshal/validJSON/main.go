package main

import (
	"encoding/json"
	"fmt"
)

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"Age": 21,
		"Username": "johndoe91",
		"Grades": null,
		"Languages": [
		  "English",
		  "French"
		]
	}`)

	isValid := json.Valid(data)
	fmt.Println(isValid)
}
