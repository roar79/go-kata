package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

type Account struct {
	IsMale bool
	Email  string
}

type Student struct {
	FirstName, lastName string
	HeightInMeters      float64
	IsMale              bool
	Profile
	Account
}

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": true,
		"Username": "johndoe91",
		"Followers": 1975,
		"Account": { "IsMale": true, "Email": "john@doe.com" }
	}`)

	var john Student

	if json.Valid(data) {
		fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	}

	fmt.Printf("%#v\n", john)
	if john.lastName == "" {
		john.lastName = ""
	}

}
