package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string `json:"uname"`
	Followers int    `json:"f_count"`
}

type Student struct {
	FirstName      string   `json:"fname"`
	LastName       string   `json:"-"` // discard
	HeightInMeters float64  `json:"height"`
	IsMale         bool     `json:"male"`
	Languages      []string `json:",omitempty"`
	Profile        Profile  `json:"profile"`
}

func main() {

	data := []byte(`
	{
		"fname": "John",
		"LastName": "Doe",
		"height": 1.75,
		"IsMale": true,
		"Languages": null,
		"profile": {
			"uname": "johndoe91",
			"Followers": 1975
		}
	}`)

	var john = Student{
		Languages: []string{"English", "French"},
	}

	if json.Valid(data) {
		fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	}
	// print `john` struct
	fmt.Printf("%#v\n", john)
}
