package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

type Student struct {
	FirstName, lastName string
	HeightInMeters      float64
	IsMale              bool
	Languages           [2]string
	Subjects            []string
	Grades              map[string]string
	Profile             Profile
}

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English", "Spanish", "German" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": { "Math": "A" },
		"Profile": {
			"Username": "johndoe91",
			"Followers": 1975
		}
	}`)

	var john = Student{
		IsMale:   true,
		Subjects: []string{"Art"},
		Grades:   map[string]string{"Science": "A+"},
	}

	if json.Valid(data) {
		fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	}

	fmt.Printf("%#v\n", john)
	if john.lastName == "" {
		john.lastName = ""
	}
}
