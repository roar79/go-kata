package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

type Student struct {
	FirstName, lastName string
	HeightInMeters      float64
	IsMale              bool
	Languages           [2]string
	Subjects            []string
	Grades              map[string]string
	Profile             *Profile
}

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": null,
		"Profile": { "Followers": 1975 }
	}`)

	var john = Student{
		IsMale:    true,
		Languages: [2]string{"Korean", "Chinese"},
		Subjects:  nil,
		Grades:    map[string]string{"Math": "A"},
		Profile:   &Profile{Username: "johndoe91"},
	}

	if json.Valid(data) {
		fmt.Printf("Error: %v\n\n", json.Unmarshal(data, &john))
	}

	fmt.Printf("%#v\n\n", john)
	fmt.Printf("%#v\n", john.Profile)
	if john.lastName == "" {
		john.lastName = ""
	}
}
