package main

import (
	"encoding/json"
	"fmt"
)

func main() {

	data := []byte(`
	{
		"id": 123,
		"fname": "John",
		"height": 1.75,
		"male": true,
		"languages": null,
		"subjects": [ "Math", "Science" ],
		"profile": {
			"uname": "johndoe91",
			"f_count": 1975
		}
	}`)

	var john interface{}
	fmt.Printf("Before: `type` of `john` is %T and its `value` is %v\n", john, john)

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	fmt.Printf("After: `type` of `john` is %T\n\n", john)

	fmt.Printf("%#v\n", john)
}
