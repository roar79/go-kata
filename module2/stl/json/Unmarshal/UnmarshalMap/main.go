package main

import (
	"encoding/json"
	"fmt"
)

type Student map[string]interface{}

func main() {

	data := []byte(`
	{
		"id": 123,
		"fname": "John",
		"height": 1.75,
		"male": true,
		"languages": null,
		"subjects": [ "Math", "Science" ],
		"profile": {
			"uname": "johndoe91",
			"f_count": 1975
		}
	}`)

	var john Student

	if json.Valid(data) {
		fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))
	}

	fmt.Printf("%#v\n\n", john)

	i := 1
	for k, v := range john {
		fmt.Printf("%d: key (`%T`)`%v`, value (`%T`)`%#v`\n", i, k, k, v, v)
		i++
	}
}
