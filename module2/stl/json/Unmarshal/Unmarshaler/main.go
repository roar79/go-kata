package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Profile struct {
	Username  string
	Followers string
}

func (p *Profile) UnmarshalJSON(data []byte) error {

	var container map[string]interface{}
	_ = json.Unmarshal(data, &container)
	fmt.Printf("container: %T / %#v\n\n", container, container)

	iuserName := container["Username"]
	ifollowers := container["f_count"]
	fmt.Printf("iuserName: %T/%#v\n", iuserName, iuserName)
	fmt.Printf("ifollowers: %T/%#v\n\n", ifollowers, ifollowers)

	userName, _ := iuserName.(string)    // get `string` value
	followers, _ := ifollowers.(float64) // get `float64` value
	fmt.Printf("userName: %T/%#v\n", userName, userName)
	fmt.Printf("followers: %T/%#v\n\n", followers, followers)

	p.Username = strings.ToUpper(userName)
	p.Followers = fmt.Sprintf("%.2fk", followers/1000)

	return nil
}

type Student struct {
	FirstName string
	Profile   Profile
}

func main() {

	data := []byte(`
	{
		"FirstName": "John",
		"Profile": {
			"Username": "johndoe91",
			"f_count": 1975
		}
	}`)

	var john Student

	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n", john)
}
