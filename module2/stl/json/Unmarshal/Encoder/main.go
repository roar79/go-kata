package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {

	buf := new(bytes.Buffer)
	bufEncoder := json.NewEncoder(buf)

	_ = bufEncoder.Encode(Person{"Ross Geller", 28})
	_ = bufEncoder.Encode(Person{"Monica Geller", 27})
	_ = bufEncoder.Encode(Person{"Jack Geller", 56})

	fmt.Println(buf)
}
