package main

import (
	"encoding/json"
	"fmt"
)

type Student struct {
	FirstName, LastName string
	Email               string
	Age                 int
	HeightInMeters      float64
	IsMale              bool
}

func main() {
	join := Student{
		FirstName:      "John",
		LastName:       "Doe",
		Age:            21,
		HeightInMeters: 1.75,
		IsMale:         true,
	}
	joinJSON, _ := json.Marshal(join)

	fmt.Println(string(joinJSON))
}
