package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	followers int
	Grades    map[string]string
}

type Student struct {
	FirstName, lastName string
	Age                 int
	Profile
	Languages []string
}

func main() {

	john := Student{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			followers: 1975,
		},
		Languages: []string{"English", "French"},
	}

	johnJSON, _ := json.MarshalIndent(john, "", "  ")

	fmt.Println(string(johnJSON))
}
