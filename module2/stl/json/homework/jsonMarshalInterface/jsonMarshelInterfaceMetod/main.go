package main

import (
	"encoding/json"
	"fmt"
)

type ProfileI interface {
	Follow()
}

type Profile struct {
	Username  string
	Followers int
}

func (p *Profile) Follow() {
	p.Followers++
}

type Student struct {
	FirstName, lastName string
	Age                 int
	Primary             ProfileI
	Secondary           ProfileI
}

func main() {

	john := &Student{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Primary: &Profile{
			Username:  "johndoe91",
			Followers: 1975,
		},
	}

	john.Primary.Follow()

	johnJSON, _ := json.MarshalIndent(john, "", "  ")

	fmt.Println(string(johnJSON))
}
