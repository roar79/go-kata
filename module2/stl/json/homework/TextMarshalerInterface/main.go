package main

import (
	"encoding/json"
	"fmt"
)

// Profile структура Профиль
type Profile struct {
	Username  string
	Followers int
}

// MarshalJSON метод Профиля для имплементации интерфейса MarshalJSON
func (p Profile) MarshalJSON() ([]byte, error) {
	// return JSON value (TODO: handle error gracefully)
	return []byte(fmt.Sprintf(`{"f_count": "%d"}`, p.Followers)), nil
}

// Age обьявление типа`Age`
type Age int

// MarshalText метад Ade для имплементации интерфейса MarshalText
func (a Age) MarshalText() ([]byte, error) {
	// return string value (TODO: handle error gracefully)
	return []byte(fmt.Sprintf(`{"age": %d}`, int(a))), nil
}

type Student struct {
	FirstName, lastName string
	Age                 Age
	Profile             Profile
}

func main() {

	john := &Student{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			Followers: 1975,
		},
	}

	johnJSON, _ := json.MarshalIndent(john, "", "  ")

	fmt.Println(string(johnJSON))
}
