package main

import "testing"

var pets Pets

var data []byte

func BenchmarkJson(b *testing.B) {

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkGithubJson(b *testing.B) {

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
