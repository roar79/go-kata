package main

import (
	"encoding/json"
	"fmt"
	"github.com/json-iterator/go"
	"os"
)

var jsonData, err = os.ReadFile("response_16.json")

type Pets []Pet

func UnmarshalPets2(jsonData []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(jsonData, &r)
	return r, err
}

func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

func UnmarshalPets(jsonData []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(jsonData, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func main() {
	var jsonData, err = os.ReadFile("response_16.json")
	if err != nil {
		fmt.Println(err)
	}

	jsonDataValid := json.Valid(jsonData)
	if !jsonDataValid {
		os.Exit(1)

	}
	jsonData1, _ := UnmarshalPets(jsonData)
	_, _ = jsonData1.Marshal()
	fmt.Println(jsonData1)

	fmt.Println("_________________________________________________")

	jsonData2, _ := UnmarshalPets2(jsonData)
	_, _ = jsonData2.Marshal2()
	fmt.Println(jsonData2)

}
