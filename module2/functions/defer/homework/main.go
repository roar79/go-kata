package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

// BEGIN (write your solution here)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	var err error

	job.Merged = make(map[string]string)

	job.IsFinished = true

	if len(job.Dicts) < 2 {
		err = errNotEnoughDicts

	} else {
		for _, dist := range job.Dicts {
			if dist != nil {
				for i, value := range dist {
					job.Merged[i] = value
					err = nil
				}

			} else {
				err = errNilDict
			}

		}

	}
	fmt.Printf("&MergeDictsJob{ %+v}, %v\n", *job, err)

	return job, err
}

func main() {

	_, _ = ExecuteMergeDictsJob(&MergeDictsJob{})
	_, _ = ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}})
	_, _ = ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}})
}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{})
//&MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"

// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}})
//&MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"

// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}})
//&MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil

//Задание: реализуй функцию ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error), которая выполняет джобу MergeDictsJob и возвращает ее.
//
//Алгоритм обработки джобы следующий:
//
//перебрать по порядку все словари job.Dicts и записать каждое ключ-значение в результирующую мапу job.Merged;
//
//если в структуре job.Dicts меньше двух словарей, возвращается ошибка errNotEnoughDicts = errors.New(at least 2 dictionaries are required);
//
//если в структуре job.Dicts встречается словарь в виде нулевого значения nil, то возвращается ошибка errNilDict = errors.New(nil dictionary);
//
//независимо от успешного выполнения или ошибки в возвращаемой структуре MergeDictsJob, поле IsFinished должно быть заполнено как true.
