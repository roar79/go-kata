package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const timeout = 30 * time.Second

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}
		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	rand.NewSource(time.Now().UnixNano())

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	ticker := time.NewTicker(timeout)
	defer ticker.Stop()

	for {
		select {
		case num, ok := <-mainChan:
			if !ok {
				fmt.Println("Все каналы закрыты")
				return
			}
			fmt.Println(num)
		case <-ticker.C:
			fmt.Println("Сработал таймаут, каналы закрыты")
			close(a)
			close(b)
			close(c)
			return
		}
	}
}
