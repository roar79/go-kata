package main

import (
	"fmt"
	"time"
)

func main() {
	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "Прошло полсекунды"

		}
	}()
	go func() {
		for {
			time.Sleep(time.Second * 2)
			message2 <- "Прошло 2 секунды"

		}
	}()
	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
		//fmt.Println(<-message1)
		//fmt.Println(<-message2)

		//	"https://platform.kata.academy",
		//	"https://go.dev/dl/",
		//	"https://yandex.ru/sea",
		//	"https://translate.yandex.ru/",
		//}
		//var wg sync.WaitGroup
		//for _, url := range urls {
		//	wg.Add(1)
		//	go func(url string) {
		//		dohHTTP(url)
		//		wg.Done()
		//	}(url)
		//
		//	wg.Wait()

	}

	//message := make(chan string, 2)
	//message <- "hello"
	//message <- "word"
	//fmt.Println(<-message)
	//
	//message <- "!"
	//
	//fmt.Println(<-message)
	//fmt.Println(<-message)
	//message := make(chan string)

	//go func() {
	//	for i := 1; i < 10; i++ {
	//		message <- fmt.Sprintf("%d", i)
	//		time.Sleep(time.Millisecond * 500)
	//
	//	}
	//	close(message)
	//
	//}()
	//for msg := range message {
	//	fmt.Println(msg)
	//	//msg, open := <-message
	//if !open {
	//	break

	//go func() {
	//	time.Sleep(2 * time.Second)
	//	message <- "hello"
	//}()
	//msg := <-message
	//fmt.Println(<-message)
	//go fmt.Println("Hello from gorutine")

	//go fmt.Println("Hello fromm main")

	//time.Sleep(time.Millisecond)
	//t := time.Now()
	//rand.NewSource(t.UnixNano())
	//
	//go parseURl("http://yandex.ru")
	//parseURl("http://youtube.ru")
}

//func dohHTTP(url string) {
//	t := time.Now()
//
//	resp, err := http.Get(url)
//	if err != nil {
//		fmt.Println("falied to get <%s>: %s\n", url, err.Error())
//
//	}
//	defer resp.Body.Close()
//	fmt.Printf("<%s>- Status Code [%d] -Latency %d ms\n", url, resp.StatusCode, time.Since(t).Milliseconds())
//}

//func parseURl(url string) {
//	for i := 0; i < 5; i++ {
//		latency := rand.Intn(500) + 500
//		time.Sleep(time.Duration(latency))
//
//		fmt.Printf("Parsing <%s> -step %d -latency A%d\n", url, i+1, latency)
//
//	}
//}
