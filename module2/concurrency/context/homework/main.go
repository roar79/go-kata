package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(ctx context.Context, chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for {
					select {
					case <-ctx.Done():
						return
					case id, ok := <-ch:
						if !ok {
							return
						}
						mergedCh <- id
					}
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData(ctx context.Context) chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case <-ctx.Done():
				return
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	rand.NewSource(time.Now().UnixNano())

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData(ctx)

	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				return
			case a <- num:
			}
		}
		close(a)
	}()

	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				return
			case b <- num:
			}
		}
		close(b)
	}()

	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				return
			case c <- num:
			}
		}
		close(c)
	}()

	mainChan := joinChannels(ctx, a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}

}
