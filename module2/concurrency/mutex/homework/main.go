package main

import (
	"fmt"
	"sync"
)

type Cache struct {
	data map[string]interface{}
	init bool
	mtx  sync.Mutex
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
	}
}

func (c *Cache) Set(key string, v interface{}) error {
	c.mtx.Lock()

	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}
	c.data[key] = v
	c.mtx.Unlock()

	return nil
}

func (c *Cache) Get(key string) interface{} {
	c.mtx.Lock()

	if !c.init {
		return nil
	}
	c.mtx.Unlock()
	return c.data[key]
}

func main() {
	cache := NewCache()
	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}

	var wg sync.WaitGroup
	for _, key := range keys {
		wg.Add(1)
		go func(k string) {
			defer wg.Done()
			err := cache.Set(k, len(k))
			if err != nil {
				return
			}
		}(key)
	}

	wg.Wait()

	//mt.Println(cache)

}
