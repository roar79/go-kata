package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is", unsafe.Sizeof(n), "bytes")
	typeInt()

}

func typeInt() {
	fmt.Println("===START type int====")
	var uintNumber8 uint8 = 1 << 7 // делаем сдвиг на середину uint8
	var min = int8(uintNumber8)    //преобразоваем в int8
	uintNumber8--                  //минусумем 1
	var max = int8(uintNumber8)    //преобразоваем в int8
	fmt.Println(min, max)          //вывод в терминал
	fmt.Println("int8 min value:", min, "int8 ma[ value:", max, "size", unsafe.Sizeof(min), "bytes")
	//остальные размеры битности
	//int16

	var uintNumber16 uint16 = 1 << 15 // делаем сдвиг на середину uint16
	var min2 = int16(uintNumber16)    //преобразоваем в int16
	uintNumber16--                    //минусумем 1
	var max2 = int16(uintNumber16)    //преобразоваем в int16
	fmt.Println(min2, max2)           //вывод в терминал
	fmt.Println("int16 min value:", min2, "int16 max value:", max2, "size", unsafe.Sizeof(min2), "bytes")
	//int32

	var uintNumber32 uint32 = 1 << 31 // делаем сдвиг на середину uint32
	var min3 = int32(uintNumber32)    //преобразоваем в int32
	uintNumber32--                    //минусумем 1
	var max3 = int32(uintNumber32)    //преобразоваем в int32
	fmt.Println(min3, max3)           //вывод в терминал
	fmt.Println("int32 min value:", min3, "int32 max value:", max3, "size", unsafe.Sizeof(min3), "bytes")
	//int64

	var uintNumber64 uint64 = 1 << 63 // делаем сдвиг на середину uint64
	var min4 = int64(uintNumber64)    //преобразоваем в int64
	uintNumber64--                    //минусумем 1
	var max4 = int64(uintNumber64)    //преобразоваем в int64
	fmt.Println(min4, max4)           //вывод в терминал
	fmt.Println("int64 min value:", min4, "int64 max value:", max4, "size", unsafe.Sizeof(min4), "bytes")
	fmt.Println("===STOP type int===")
}
