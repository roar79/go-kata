package main

import (
	"fmt"
	"unsafe"
)

func typeBool() {
	var b bool
	fmt.Println("зазмер в байтах:", unsafe.Sizeof(b))
	var u uint8 = 1
	fmt.Println(b)
	b = *(*bool)(unsafe.Pointer(&u))
	fmt.Println(b)

}

func main() {
	typeBool()
}
