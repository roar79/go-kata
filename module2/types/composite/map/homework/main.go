package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/docker/compose-cli",
			Stars: 897,
		},
		{
			Name:  "https://github.com/docker/libchan",
			Stars: 2500,
		},
		{
			Name:  "https://github.com/golang/go",
			Stars: 108000,
		},
		{
			Name:  "https://github.com/json-iterator/go",
			Stars: 11700,
		},
		{
			Name:  "https://github.com/siddontang/go",
			Stars: 429,
		},
		{
			Name:  "https://github.com/golang/oauth2",
			Stars: 4500,
		},
		{
			Name:  "https://github.com/Dreamacro/clash",
			Stars: 35600,
		},
		{
			Name:  "https://github.com/miekg/dns",
			Stars: 6700,
		},
		{
			Name:  "https://github.com/kubernetes/kubernetes",
			Stars: 95300,
		},
		{
			Name:  "https://github.com/golangci/golangci-lint",
			Stars: 11800,
		},
		{
			Name:  "https://github.com/StephenGrider/GoCasts",
			Stars: 1900,
		},
		{
			Name:  "https://github.com/segmentio/kafka-go",
			Stars: 5700,
		},
	}

	githubMap := make(map[string]Project)
	for _, v := range projects {
		githubMap[v.Name] = v
	}

	for k, p := range githubMap {
		fmt.Println(k, p)
	}
}
