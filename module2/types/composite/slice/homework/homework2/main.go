package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}

	old := 40
	users = deleteOld(users, old)
	fmt.Println(users)
}

func deleteOld(users []User, old int) []User {

	for i, num := range users {
		if num.Age > old {

			users = append(users[:i], users[i+1:]...)

			if i > len(users)-1 {
				break
			}

		}
	}
	return users
}
