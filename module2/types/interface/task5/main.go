package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	var i Userer
	var a User
	_ = i
	_ = a
	fmt.Println("Success!")
}
