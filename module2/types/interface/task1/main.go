package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n MyInterface
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch r.(type) {
	case nil:
		fmt.Println("Success!")
	default:
		fmt.Printf("%T", r)
	}
}
