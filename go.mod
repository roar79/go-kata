module gitlab.com/roar79/go-kata

go 1.19

require gitlab.com/roar79/greet v0.0.0-20230208194747-f8e8d1ec4675

require (
	github.com/brianvoe/gofakeit/v6 v6.20.1
	github.com/essentialkaos/translit v2.0.3+incompatible
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
)

require (
	github.com/kr/pretty v0.3.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	pkg.re/essentialkaos/check.v1 v1.2.0 // indirect
)
