package main

func minPartitions(n string) int {
	count := 0
	for _, v := range n {
		a := 0
		a = int(v - '0')
		if count < a {
			count = a
		}
	}
	return count
}

//func main() {
//
//}
