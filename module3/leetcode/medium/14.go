package main

func xorQueries(arr []int, queries [][]int) []int {
	prefixXOR := make([]int, len(arr))
	prefixXOR[0] = arr[0]
	for i := 1; i < len(arr); i++ {
		prefixXOR[i] = prefixXOR[i-1] ^ arr[i]
	}

	res := make([]int, len(queries))
	for i, q := range queries {
		left, right := q[0], q[1]
		if left == 0 {
			res[i] = prefixXOR[right]
		} else {
			res[i] = prefixXOR[right] ^ prefixXOR[left-1]
		}
	}

	return res
}

//func main() {
//	arr := []int{1, 3, 4, 8}
//	queries := [][]int{{0, 1}, {1, 2}, {0, 3}, {3, 3}}
//	res := xorQueries(arr, queries)
//	fmt.Println(res)
//
//}
