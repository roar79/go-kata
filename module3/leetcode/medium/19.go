package main

// это решение нашел в интернете, долго думал и разобрался. Можно не засчитывать как самостоятельную работу. Но как пример удалять не буду
type TreeNode3 struct {
	Val   int
	Left  *TreeNode3
	Right *TreeNode3
}

func averageOfSubtree(root *TreeNode3) int {
	ans := 0
	helper(root, &ans)
	return ans
}

func helper(node *TreeNode3, n *int) []int {
	if node == nil {
		return []int{0, 0}
	}
	l := helper(node.Left, n)
	r := helper(node.Right, n)
	sum := l[0] + r[0] + node.Val
	num := l[1] + r[1] + 1
	if sum/num == node.Val {
		(*n)++
	}
	return []int{sum, num}
}

//func main() {
//	// Создание бинарного дерева.
//	root := &TreeNode3{Val: 4}
//	root.Left = &TreeNode3{Val: 8}
//	root.Right = &TreeNode3{Val: 5}
//	root.Left.Left = &TreeNode3{Val: 0}
//	root.Left.Right = &TreeNode3{Val: 1}
//	root.Right.Right = &TreeNode3{Val: 6}
//	b := averageOfSubtree(root)
//	fmt.Println(b)
//}
// это решение нашел в интернете, долго думал и разобрался. Можно не засчитывать как самостоятельную работу. Но как пример удалять не буду
