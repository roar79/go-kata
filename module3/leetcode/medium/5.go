package main

type ListNode3 struct {
	Val  int
	Next *ListNode3
}

func listNodToSlice(head *ListNode3) []int {

	var result []int
	current := head
	for current != nil {
		result = append(result, current.Val)
		current = current.Next
	}
	return result
}

func pairSum(head *ListNode3) int {
	summ := 0
	slice := listNodToSlice(head)
	for i, j := 0, len(slice)-1; i < j; {
		summpair := slice[i] + slice[j]
		if summpair > summ {
			summ = summpair
		}
		i++
		j--
	}
	return summ
}

//func main() {
//
//}
