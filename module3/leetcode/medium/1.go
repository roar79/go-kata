package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	deepestLevel := getDeepestLevel(root)
	return getLevelSum(root, deepestLevel, 1)
}

func getDeepestLevel(root *TreeNode) int {
	if root == nil {
		return 0
	}
	leftLevel := getDeepestLevel(root.Left)
	rightLevel := getDeepestLevel(root.Right)
	if leftLevel > rightLevel {
		return leftLevel + 1
	} else {
		return rightLevel + 1
	}
}

func getLevelSum(root *TreeNode, deepestLevel, currentLevel int) int {
	if root == nil {
		return 0
	}
	if currentLevel == deepestLevel {
		return root.Val
	}
	leftSum := getLevelSum(root.Left, deepestLevel, currentLevel+1)
	rightSum := getLevelSum(root.Right, deepestLevel, currentLevel+1)
	return leftSum + rightSum
}

//
//func main() {
//
//}
