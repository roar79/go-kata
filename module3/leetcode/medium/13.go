package main

import "math"

func maxSum(grid [][]int) int {
	maxSum := math.MinInt32
	for i := 1; i < len(grid)-1; i++ {
		for j := 1; j < len(grid[0])-1; j++ {
			sum := grid[i][j] + grid[i-1][j-1] + grid[i-1][j] + grid[i-1][j+1] + grid[i+1][j-1] + grid[i+1][j] + grid[i+1][j+1]
			if sum > maxSum {
				maxSum = sum
			}
		}
	}
	return maxSum
}

//func main() {
//
//}
