package main

func moveToFront(slice []int, index int) []int {
	if index > 0 && index < len(slice) {
		elem := slice[index]
		copy(slice[1:index+1], slice[0:index])
		slice[0] = elem
	}
	return slice
}
func makeSlice(m int) []int {
	pslise := make([]int, m)
	for m != 0 {
		pslise[m-1] = m
		m--

	}
	return pslise

}
func findIndex(pslise []int, v int) int {
	var index int
	for i, value := range pslise {
		if value == v {
			index = i
		}
	}
	return index
}
func processQueries(queries []int, m int) []int {
	result := make([]int, 0, m)
	pslise := makeSlice(m)
	for _, v := range queries {
		index := findIndex(pslise, v)
		result = append(result, index)
		pslise = moveToFront(pslise, index)
	}
	return result
}

//func main() {
//
//	a := processQueries([]int{3, 1, 2, 1}, 5)
//	fmt.Println(a)
//}
