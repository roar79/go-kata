package main

func numTilePossibilities(tiles string) int {
	combinations := make(map[string]bool)
	countCombinations(tiles, "", combinations)
	return len(combinations)
}

func countCombinations(tiles string, currentCombination string, combinations map[string]bool) {
	if currentCombination != "" {
		combinations[currentCombination] = true
	}
	for i := 0; i < len(tiles); i++ {
		newCombination := currentCombination + string(tiles[i])
		newTiles := tiles[:i] + tiles[i+1:]
		countCombinations(newTiles, newCombination, combinations)
	}
}

//func main() {
//
//}
