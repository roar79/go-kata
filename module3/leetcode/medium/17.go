package main

import "math"

func countPointsInCircle(points [][]int, query []int) int {
	x, y, r := query[0], query[1], query[2]
	count := 0
	for _, point := range points {
		distance := math.Sqrt(math.Pow(float64(point[0]-x), 2) + math.Pow(float64(point[1]-y), 2))
		if distance <= float64(r) {
			count++
		}
	}
	return count
}

func countPoints(points [][]int, queries [][]int) []int {
	var result []int
	for _, query := range queries {
		result = append(result, countPointsInCircle(points, query))
	}
	return result
}

//func main() {
//
//}
