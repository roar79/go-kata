package main

type TreeNode4 struct {
	Val   int
	Left  *TreeNode4
	Right *TreeNode4
}

func constructMaximumBinaryTree(nums []int) *TreeNode4 {
	if len(nums) == 0 {
		return nil
	}
	maxIndex := 0
	for i := 1; i < len(nums); i++ {
		if nums[i] > nums[maxIndex] {
			maxIndex = i
		}
	}
	root := &TreeNode4{Val: nums[maxIndex]}
	root.Left = constructMaximumBinaryTree(nums[:maxIndex])
	root.Right = constructMaximumBinaryTree(nums[maxIndex+1:])
	return root
}

//func main() {
//
//}
