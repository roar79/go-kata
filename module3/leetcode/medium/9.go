package main

import (
	"fmt"
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	ans := make([]bool, len(l))
	for i := 0; i < len(l); i++ {
		if r[i]-l[i] < 2 {
			ans[i] = true
			continue
		}
		arr := make([]int, r[i]-l[i]+1)
		copy(arr, nums[l[i]:r[i]+1])
		fmt.Println("-------------------------------------")
		fmt.Println(arr)

		sort.Ints(arr)
		fmt.Println(arr)
		found := false
		for j := 2; j < len(arr); j++ {
			if arr[j]-arr[j-1] != arr[j-1]-arr[j-2] {
				found = false
				break
			}
			found = true
		}
		ans[i] = found
	}
	return ans
}

//func main() {
//	nuns := []int{-12, -9, -3, -12, -6, 15, 20, -25, -20, -15, -10}
//	l := []int{0, 1, 6, 4, 8, 7}
//	r := []int{4, 4, 9, 7, 9, 10}
//	fmt.Println(checkArithmeticSubarrays(nuns, l, r))
//}
