package main

import (
	"sort"
)

func sortTheStudents(score [][]int, k int) [][]int {
	kScores := make([]int, len(score))
	for i := range score {
		kScores[i] = score[i][k]
	}

	idxs := make([]int, len(score))
	for i := range idxs {
		idxs[i] = i
	}
	sort.Slice(idxs, func(i, j int) bool {
		return kScores[idxs[i]] > kScores[idxs[j]]
	})

	sortedScore := make([][]int, len(score))
	for i, idx := range idxs {
		sortedScore[i] = score[idx]
	}
	return sortedScore
}

//func main() {
//	a := [][]int{{10, 6, 9, 1}, {7, 5, 11, 2}, {4, 8, 3, 15}}
//	k := 2
//	fmt.Println(sortTheStudents(a, k))
//}
