package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func sumList(head *ListNode) *ListNode {
	sum := 0
	curr := head
	var newHead, newTail *ListNode
	for curr != nil {
		sum += curr.Val
		if curr.Next == nil || curr.Next.Val == 0 {
			if sum > 0 {
				if newHead == nil {
					newHead = &ListNode{Val: sum}
					newTail = newHead
				} else {
					newTail.Next = &ListNode{Val: sum}
					newTail = newTail.Next
				}
			}
			sum = 0
		}
		curr = curr.Next
	}
	return newHead
}

//func main() {
//
//}
