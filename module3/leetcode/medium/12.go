package main

type ListNode2 struct {
	Val  int
	Next *ListNode2
}

func mergeInBetween(list1 *ListNode2, a int, b int, list2 *ListNode2) *ListNode2 {

	if list1 == nil {
		return list2
	}
	if list2 == nil {
		return list1
	}

	prev := (*ListNode2)(nil)
	curr := list1
	for i := 0; i < a; i++ {
		prev = curr
		curr = curr.Next
	}

	end := curr
	for i := 0; i < b-a+1; i++ {
		end = end.Next
	}

	if prev != nil {
		prev.Next = list2
	} else {
		list1 = list2
	}

	lastNode := list2
	for lastNode.Next != nil {
		lastNode = lastNode.Next
	}

	lastNode.Next = end

	return list1
}

//func main() {
//
//}
