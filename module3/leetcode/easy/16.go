package main

func smallestEvenMultiple(n int) int {

	if n%2 != 0 {
		n = n * 2
	}
	return n
}

//func main() {
//	n := 39
//	fmt.Println(smallestEvenMultiple(n))
//}
