package main

import (
	"fmt"
	"strconv"
)

func differenceOfSum(nums []int) int {
	summ := 0
	for _, num := range nums {
		summ += num
	}
	str := fmt.Sprintf("%v", nums)

	summ2 := 0
	for _, num2 := range str {
		strGigital, _ := strconv.Atoi(string(num2))
		summ2 += strGigital
	}
	result := summ - summ2
	return result

}

//func main() {
//
//}
//
