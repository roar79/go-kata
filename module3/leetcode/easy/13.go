package main

func numJewelsInStones(jewels string, stones string) int {
	jewelsSet := make(map[rune]bool)
	for _, jewel := range jewels {
		jewelsSet[jewel] = true
	}
	count := 0
	for _, stone := range stones {
		if jewelsSet[stone] {
			count++
		}
	}
	return count
}

//func main() {
//
//	jewels := "aA"
//	stones := "aAAbbbb"
//	fmt.Println(numJewelsInStones(jewels, stones))
//
//}
