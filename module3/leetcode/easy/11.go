package main

func runningSum(nums []int) []int {
	result := make([]int, len(nums))
	var k int
	for i, v := range nums {
		k += v
		result[i] = k

	}
	return result
}

//func main() {
//	w := []int{1, 1, 1, 1, 1}
//	fmt.Println(runningSum(w))
//}
