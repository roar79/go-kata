package main

func decode(encoded []int, first int) []int {
	n := len(encoded) + 1
	arr := make([]int, n)
	arr[0] = first
	for i := 0; i < len(encoded); i++ {
		arr[i+1] = encoded[i] ^ arr[i]
	}
	return arr
}

//func main() {
//
//}
