package main

import (
	"fmt"
	"sort"
)

func minimumSum(num int) int {
	digits := make([]int, 0, 4)
	for _, digit := range fmt.Sprint(num) {
		digits = append(digits, int(digit-'0'))
	}
	sort.Ints(digits)
	new1, new2 := make([]int, 0, 2), make([]int, 0, 2)
	for i := 0; i < len(digits); {
		new1 = append(new1, digits[i])
		new2 = append(new2, digits[i+1])
		i = i + 2
	}
	result1 := 0
	for i := 0; i < len(new1); i++ {
		result1 = result1*10 + new1[i]
	}
	result2 := 0
	for i := 0; i < len(new2); i++ {
		result2 = result2*10 + new2[i]
	}

	return result1 + result2
}

//func main()
//	{
//
