package main

import (
	"fmt"
	"sort"
)

func sortByHeight(names []string, heights []int) []string {

	hMap := make(map[int]string)
	for i, h := range heights {
		hMap[h] = names[i]
	}

	sort.Sort(sort.Reverse(sort.IntSlice(heights)))

	res := make([]string, len(names))
	for i, h := range heights {
		res[i] = hMap[h]
	}

	return res
}

func main() {
	names := []string{"Mary", "John", "Emma"}
	heights := []int{180, 165, 170}
	fmt.Println(sortByHeight(names, heights))

}
