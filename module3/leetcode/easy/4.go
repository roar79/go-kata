package main

func buildArray(nums []int) []int {
	ans := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		ans[i] = nums[nums[i]]
	}
	return ans

}

//func main() {
//	num := []int{0, 2, 1, 5, 3, 4}
//	a := buildArray(num)
//	fmt.Println(a)
//}
