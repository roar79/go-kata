package main

func kidsWithCandies(candies []int, extraCandies int) []bool {
	result := make([]bool, len(candies))
	for i := 0; i < len(candies); i++ {
		max := true
		for j := 0; j < len(candies); j++ {
			if candies[i]+extraCandies < candies[j] {
				max = false
				break
			}
		}
		result[i] = max
	}
	return result
}

//func main() {
//
//}
