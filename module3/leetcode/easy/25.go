package main

func createTargetArray(nums []int, index []int) []int {
	n := len(nums)
	target := make([]int, n)
	for i := 0; i < n; i++ {
		idx := index[i]
		val := nums[i]
		copy(target[idx+1:i+1], target[idx:i])
		target[idx] = val
	}
	return target
}

//func main() {
//
//}
