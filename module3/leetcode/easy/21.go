package main

import "fmt"

func subtractProductAndSum(n int) int {
	sum := 0
	multiplication := 1
	for _, v := range fmt.Sprint(n) {
		//	fmt.Println(v - '0')
		sum += int(v - '0')
		multiplication *= int(v - '0')

	}
	return multiplication - sum

}

//func main() {
//	fmt.Println(subtractProductAndSum(1234))
//}
