package main

func mostWordsFound(sentences []string) int {
	result := 0

	for _, sentence := range sentences {
		sentenceRunes := []rune(sentence)
		count := 0
		for _, sentenceRune := range sentenceRunes {
			if sentenceRune == ' ' {
				count++
			}
		}
		if result < count {
			result = count
		}
	}
	return result

}

//
//func main() {
//
//}
