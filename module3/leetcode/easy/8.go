package main

func findKthPositive(arr []int, k int) int {
	i, missing := 0, 1
	for k > 0 {
		if i < len(arr) && arr[i] == missing {
			i++
		} else {
			k--
		}
		if k == 0 {
			break
		}
		missing++
	}
	return missing
}

//func main() {
//
//}
