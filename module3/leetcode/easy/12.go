package main

func numIdenticalPairs(nums []int) int {
	freq := make(map[int]int)
	for _, num := range nums {
		freq[num]++
	}
	goodPairs := 0
	for _, count := range freq {
		goodPairs += count * (count - 1) / 2
	}
	return goodPairs
}

//func main() {
//	w := []int{1, 2, 3, 1, 1, 3}
//	fmt.Println(numIdenticalPairs(w))
//}
