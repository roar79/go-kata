package main

func balancedStringSplit(s string) int {
	count := 0
	balance := 0
	for _, ch := range s {
		if ch == 'R' {
			balance++
		} else {
			balance--
		}
		if balance == 0 {
			count++
		}
	}
	return count
}

//func main() {
//	s := "RLRRLLRLRL"
//	count := balancedStringSplit(s)
//	fmt.Println(count)
//	s = "RLRRRLLRLL"
//	count = balancedStringSplit(s)
//	fmt.Println(count)
//	s = "LLLLRRRR"
//	count = balancedStringSplit(s)
//	fmt.Println(count)
//
//}
