package main

var (
	morseITU = map[string]string{
		"a": ".-",
		"b": "-...",
		"c": "-.-.",
		"d": "-..",
		"e": ".",
		"f": "..-.",
		"g": "--.",
		"h": "....",
		"i": "..",
		"j": ".---",
		"k": "-.-",
		"l": ".-..",
		"m": "--",
		"n": "-.",
		"o": "---",
		"p": ".--.",
		"q": "--.-",
		"r": ".-.",
		"s": "...",
		"t": "-",
		"u": "..-",
		"v": "...-",
		"w": ".--",
		"x": "-..-",
		"y": "-.--",
		"z": "--..",
	}
)

func uniqueMorseRepresentations(words []string) int {
	uniqueTransformations := make(map[string]bool)
	for _, word := range words {
		morseWord := ""
		for _, letter := range word {
			morseWord += morseITU[string(letter)]
		}
		uniqueTransformations[morseWord] = true
	}

	return len(uniqueTransformations)
}

//func main() {
//	a := []string{"gin", "zen", "gig", "msg"}
//	fmt.Println(uniqueMorseRepresentations(a))
//}
