package main

import "fmt"

func countDigits(num int) int {
	count := 0
	for _, v := range fmt.Sprint(num) {
		if num%int(v-'0') == 0 {
			count++
		}

	}
	return count
}

//func main() {
//	fmt.Println(countDigits(7))
//	fmt.Println(countDigits(121))
//
//	fmt.Println(countDigits(1248))
//
//}
