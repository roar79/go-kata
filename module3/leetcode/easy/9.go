package main

func finalValueAfterOperations(operations []string) int {
	var result int = 0
	for _, a := range operations {
		switch a {
		case "++ X":
			result++
		case "X ++":
			result++
		case "X--":
			result--
		case "--X":
			result--
		}
	}
	return result

}

//func main() {
//	operations := []string{"--X", "X ++", "X ++"}
//	fmt.Println(finalValueAfterOperations(operations))
//
//}
