package main

func shuffle(nums []int, n int) []int {
	result := make([]int, len(nums))
	i, j, k := 0, n, 0
	for i < n && j < len(nums) {
		result[k] = nums[i]
		result[k+1] = nums[j]
		i++
		j++
		k += 2
	}
	return result
}

//func main() {
//	nums := []int{2, 5, 1, 3, 4, 7}
//	n := 3
//	result := shuffle(nums, n)
//	fmt.Println(result)
//}
