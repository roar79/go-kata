package main

func defangIPaddr(address string) string {
	var chage string
	for _, char := range address {
		if char == '.' {
			chage += "[.]"
		} else {
			chage += string(char)
		}
	}
	return chage
}

//func main() {
//	a := "1.1.1.1"
//	fmt.Println(defangIPaddr(a))
//
//}
//
