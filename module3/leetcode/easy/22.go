package main

func smallerNumbersThanCurrent(nums []int) []int {
	result := make([]int, 0, len(nums))
	for _, v := range nums {
		count := 0
		for _, z := range nums {
			if v > z {
				count++
			}
		}
		result = append(result, count)
	}
	return result
}

//func main() {
//	a := []int{8, 1, 2, 2, 3}
//	fmt.Println(smallerNumbersThanCurrent(a))
//}
