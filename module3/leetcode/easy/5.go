package main

func numberOfMatches(n int) int {
	var match int = 0
	for n > 1 {
		match += n / 2
		n = n/2 + n%2

	}
	return match
}

//func main() {
//	n := 14
//
//	fmt.Println(numberOfMatches(n))
//}
