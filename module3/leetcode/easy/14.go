package main

func maximumWealth(accounts [][]int) int {
	max := 0
	for _, account := range accounts {
		count := 0
		for _, value := range account {
			count += value
		}
		if count > max {
			max = count
		}

	}
	return max
}

//func main() {
//	accounts := [][]int{{1, 2, 3}, {3, 2, 1}}
//	fmt.Println(maximumWealth(accounts))
//}
