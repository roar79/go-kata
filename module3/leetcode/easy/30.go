package main

import (
	"math"
)

func countGoodTriplets(arr []int, a int, b int, c int) int {
	n := len(arr)
	count := 0
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			for k := j + 1; k < n; k++ {
				if abs(arr[i]-arr[j]) <= a && abs(arr[j]-arr[k]) <= b && abs(arr[i]-arr[k]) <= c {
					count++
				}
			}
		}
	}
	return count
}

func abs(x int) int {
	return int(math.Abs(float64(x)))
}

//func main() {
//	arr := []int{3, 0, 1, 1, 9, 7}
//	a, b, c := 7, 2, 3
//	fmt.Println(countGoodTriplets(arr, a, b, c)) // Output: 4
//}
