package main

import (
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int // we'll use it later
	start  *Post
	end    *Post
}

// Append Реализуйте метод Append для добавления нового поста в конец потока
func (f *Feed) Append(newPost *Post) {
	if f.start == nil {
		f.start = newPost
		f.end = newPost
	} else {
		f.end.next = newPost
		f.end = newPost
	}
	f.length++
	// ваш код здесь
}

// Remove Реализуйте метод Remove для удаления поста по дате публикации из потока
func (f *Feed) Remove(publishDate int64) {
	var prev *Post
	curr := f.start
	for curr != nil {
		if curr.publishDate == publishDate {
			if prev == nil {
				f.start = curr.next
			} else {
				prev.next = curr.next
			}
			if curr.next == nil {
				f.end = prev
			}
			f.length--
			break
		}
		prev = curr
		curr = curr.next
	}

	// ваш код здесь
}

// Insert Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации
func (f *Feed) Insert(newPost *Post) {
	if f.start == nil || f.start.publishDate >= newPost.publishDate {
		newPost.next = f.start
		f.start = newPost
		if f.end == nil {
			f.end = newPost
		}
	} else {
		curr := f.start
		for curr.next != nil && curr.next.publishDate < newPost.publishDate {
			curr = curr.next
		}
		newPost.next = curr.next
		curr.next = newPost
		if curr == f.end {
			f.end = newPost
		}
	}
	f.length++
	// ваш код здесь
}

// Inspect Реализуйте метод Inspect для вывода информации о потоке и его постах
func (f *Feed) Inspect() {
	fmt.Printf("Feed (length=%d):\n", f.length)
	curr := f.start
	i := 1
	for curr != nil {
		fmt.Printf("Item: %d- %s (%v %p)\n", i, curr.body, time.Unix(curr.publishDate, 0), curr)
		curr = curr.next
		i++
	}
	// ваш код здесь
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()
	//f.Remove()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()
}
