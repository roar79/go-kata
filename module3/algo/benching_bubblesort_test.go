package algo

import (
	"testing"
)

func BenchmarkBubbleSort(b *testing.B) {
	data := []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}

}
