package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
	InsertFirst(data *Commit)
	InsertLast(data *Commit)
}
type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func quickSort(data []*Commit) []*Commit {
	if len(data) <= 1 {
		return data
	}

	pivotIndex := len(data) / 2
	pivot := data[pivotIndex]

	left := make([]*Commit, 0, len(data))
	right := make([]*Commit, 0, len(data))
	for i := 0; i < len(data); i++ {
		if i == pivotIndex {
			continue
		}
		if data[i].Date.Before(pivot.Date) {
			left = append(left, data[i])
		} else {
			right = append(right, data[i])
		}
	}

	left = quickSort(left)
	right = quickSort(right)

	return append(append(left, pivot), right...)
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) {
	data, err := os.ReadFile(path)
	if err != nil {
		_ = fmt.Errorf("failed to read JSON file: %s", err)
	}
	jsonDataValid := json.Valid(data)
	if !jsonDataValid {
		os.Exit(1)
	}

	var commits []*Commit
	err = json.Unmarshal(data, &commits)
	if err != nil {
		_ = fmt.Errorf("failed to unmarshal JSON: %s", err)
	}
	commits = quickSort(commits)
	for _, c := range commits {
		d.InsertLast(c)
	}

	return
}

// InsertFirst вставка элемента в начало списка
func (d *DoubleLinkedList) InsertFirst(data *Commit) {
	node := &Node{
		data: data,
		prev: nil,
		next: d.head,
	}

	if d.head == nil {
		d.tail = node
	} else {
		d.head.prev = node
	}

	d.head = node
	d.len++
}

// InsertLast вставка элемента в конец списка
func (d *DoubleLinkedList) InsertLast(data *Commit) {
	node := &Node{
		data: data,
		prev: d.tail,
		next: nil,
	}

	if d.tail == nil {
		d.head = node
	} else {
		d.tail.next = node
	}

	d.tail = node
	d.len++
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		d.curr = d.head
	} else {
		d.curr = d.curr.next
	}

	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil {
		d.curr = d.tail
	} else {
		d.curr = d.curr.prev
	}

	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n < 0 || n > d.len {
		return fmt.Errorf("index out of range")
	}

	node := &Node{data: &c}

	if n == 0 {
		if d.head == nil {
			d.head = node
			d.tail = node
			d.curr = node
		} else {
			node.next = d.head
			d.head.prev = node
			d.head = node
		}
	} else if n == d.len {
		d.tail.next = node
		node.prev = d.tail
		d.tail = node
	} else {
		curr := d.head
		for i := 0; i < n-1; i++ {
			curr = curr.next
		}
		node.next = curr.next
		node.prev = curr
		curr.next.prev = node
		curr.next = node
	}
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n < 0 || n >= d.len {
		return fmt.Errorf("index out of range")
	}

	if n == 0 {
		if d.head.next == nil {
			d.curr = nil
			d.tail = nil
		} else {
			d.head = d.head.next
			d.head.prev = nil
			d.curr = d.head
		}
	} else if n == d.len-1 {
		d.tail = d.tail.prev
		d.tail.next = d.curr
	}
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf("current node is nil")
	}

	if d.curr == d.head {
		d.head = d.curr.next
		if d.head != nil {
			d.head.prev = nil
		}
		d.curr = d.head
	} else if d.curr == d.tail {
		d.tail = d.curr.prev
		d.tail.next = nil
		d.curr = nil
	} else {
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
		d.curr = d.curr.next
	}

	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, fmt.Errorf("current node is nil")
	}

	curr := d.head
	index := 0
	for curr != d.curr && curr != nil {
		curr = curr.next
		index++
	}

	if curr == nil {
		return -1, fmt.Errorf("current node not found")
	}

	return index, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.tail == nil {
		return nil
	}

	node := d.tail
	if d.head == d.tail {
		d.head = nil
		d.curr = nil
	} else {
		d.tail = d.tail.prev
		d.tail.next = nil
	}

	d.len--
	return node
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.head == nil {
		return nil
	}

	node := d.head
	if d.head == d.tail {
		d.tail = nil
		d.curr = nil
	} else {
		d.head = d.head.next
		d.head.prev = nil
	}

	d.len--
	return node
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	curr := d.head
	for curr != nil {
		if curr.data.UUID == uuID {
			return curr
		}
		curr = curr.next
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(msg string) *Node {
	curr := d.head
	for curr != nil {
		if curr.data.Message == msg {
			return curr
		}
		curr = curr.next
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.len <= 1 {
		return d
	}

	curr := d.head
	d.head, d.tail = d.tail, d.head
	for curr != nil {
		curr.prev, curr.next = curr.next, curr.prev
		curr = curr.prev
	}

	return d
}

func GenerateJSON(count int, filePath string) error {
	commits := make([]Commit, count)
	for i := 0; i < count; i++ {
		commits[i] = Commit{
			Message: gofakeit.Sentence(10),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		}
	}

	jsonBytes, err := json.Marshal(commits)
	if err != nil {
		return fmt.Errorf("failed to marshal commits to JSON: %s", err)
	}

	file, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("failed to create file: %s", err)
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {

		}
	}(file)

	_, err = file.Write(jsonBytes)
	if err != nil {
		return fmt.Errorf("failed to write JSON to file: %s", err)
	}

	return nil
}

// Дополнительное задание написать генератор данных
// используя библиотеку gofakeit
func main() {
	_ = GenerateJSON(100, "module3/task/qwerty.json")
	var dll DoubleLinkedList
	dll.Len()
	dll.LoadData("module3/task/qwerty.json")

	fmt.Println(dll.Len())
	fmt.Println(dll.Current())
	a := dll.Prev()
	fmt.Println(a.data.Message)
	dll.Reverse()

}
