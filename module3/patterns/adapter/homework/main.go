// You can edit this code!
// Click here and start typing.
package main

import "fmt"

// Falcon 9 с грузом.
type falcon9Rocket struct {
	payload []interface{}
}

// Интерфейс satellite определяет, что объекты, которые
// реализуют этот интерфейс,
// могут быть вставлены в порты Falcon9.
type satellite interface {
	insertSatelliteIntoStarlinkPort(f9r *falcon9Rocket)
}

// Функция  добавляет спутник в порт Starlink ракеты и в груз Falcon9.
func (f9r *falcon9Rocket) insertSatelliteIntoStarlinkPort(s satellite) {
	fmt.Println("Attaching satellite to Falcon 9 Rocket.")
	s.insertSatelliteIntoStarlinkPort(f9r)
}

// ыводит в консоль  данные о грузе Falcon 9.
func (f9r *falcon9Rocket) getPayload() {
	fmt.Printf("Falcon 9 payload: %s\n", f9r.payload)
}

// Спутник Starlink
type starlinkSatellite struct {
	name string
}

// Спутник oco2
type oco2Satellite struct {
	name string
}

// Создание спутника Starlink
func newStarlinkSatellite() *starlinkSatellite {
	return &starlinkSatellite{
		name: "Starlink Satellite",
	}
}

// Создание спутника ОСО2
func newOco2Satellite() *oco2Satellite {
	return &oco2Satellite{
		name: "OCO2 Satellite",
	}
}

// Добавляет Starlink в Falcon9
func (sls *starlinkSatellite) insertSatelliteIntoStarlinkPort(f9r *falcon9Rocket) {
	f9r.payload = append(f9r.payload, sls)
	fmt.Println("Starlink satellite is attached to Falcon 9 Rocket.")
}

// Добавляет OCO2 в Falcon9
func (oco2s *oco2Satellite) insertSatelliteIntoOco2Port(f9r *falcon9Rocket) {
	f9r.payload = append(f9r.payload, oco2s)
	fmt.Println("OCO2 satellite is attached to Falcon 9 Rocket.")
}

// Тип адаптер
type oco2SatelliteAdapter struct {
	oco2Satellite *oco2Satellite
}

// функция адаптора
func (oco2sa *oco2SatelliteAdapter) insertSatelliteIntoStarlinkPort(f9r *falcon9Rocket) {
	fmt.Println("Satellite adapter converts Starlink port to OCO2 port.")
	oco2sa.oco2Satellite.insertSatelliteIntoOco2Port(f9r)
}

func main() {
	f9r := &falcon9Rocket{}
	f9r.getPayload()
	sls := newStarlinkSatellite()
	f9r.insertSatelliteIntoStarlinkPort(sls)

	oco2s := newOco2Satellite()
	oco2sa := &oco2SatelliteAdapter{oco2s}
	f9r.insertSatelliteIntoStarlinkPort(oco2sa)

	f9r.getPayload()
}

/**
Последовательность:

Falcon 9 payload: []
Attaching satellite to Falcon 9 Rocket.
Starlink satellite is attached to Falcon 9 Rocket.
Attaching satellite to Falcon 9 Rocket.
Satellite adapter converts Starlink port to OCO2 port.
OCO2 satellite is attached to Falcon 9 Rocket.
Falcon 9 payload: [%!s(*main.starlinkSatellite=&{Starlink Satellite}) %!s(*main.oco2Satellite=&{OCO2 Satellite})]
**/
